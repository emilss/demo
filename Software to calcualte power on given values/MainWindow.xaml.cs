﻿using System.Windows;
using System.Windows.Media;
using System.Threading;



namespace Power_Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        // function to calculate Power
        private string CalculatePower(double magi, double mmag, int pps)
        {
            double result;            
            if (mmag <= 28)
            {
                result = ((-0.000302947 * (4.5 * mmag + 4.5) + 0.0527156) * magi + (0.362882 * (4.5 * mmag + 4.5) - 10.2579)) * magi * pps * 0.0000032;
            }
            else if (mmag > 28 && mmag <= 30)
            {
                result = ((0.000899708 * (4.5 * mmag + 4.5) - 0.10363) * magi + (0.185 * (4.5 * mmag + 4.5) + 12.8667)) * magi * pps * 0.0000032;
            }
            else if (mmag > 30 && mmag <= 32)
            {
                result = ((0.000433079 * (4.5 * mmag + 4.5) - 0.0383015) * magi + (0.28166 * (4.5 * mmag + 4.5) - 0.6657)) * magi * pps * 0.0000032;
            }
            else
            {
                result = ((0.000499574 * (4.5 * mmag + 4.5) - 0.0482757) * magi + (0.275 * (4.5 * mmag + 4.5) + 0.3333)) * magi * pps * 0.0000032;
            }
            return System.Convert.ToString(System.Math.Round(result, 2));
        }

        // function to calculate power PRF
        private string CalculatePowerPRF(double power, double prf)
        {
            return System.Convert.ToString(System.Math.Round((1 / (1 + (prf / 250))) * power, 2));  
        }

        //function to check input
        private void CheckInput(double magi, double mmag)
        {
            double[] m_mag_ranges = new double[45];
            double[] mag_i_ranges_a = { 124, 124, 128, 130, 130, 130, 134, 134, 134, 136, 138, 140, 140, 140, 144, 146, 150, 150, 150, 150, 154, 154, 156, 158, 160, 160, 164, 166, 170, 172, 174, 176, 180, 182, 184, 186, 190, 192, 194, 196, 200, 202, 203, 206, 210 };
            double[] mag_i_ranges_b = { 136, 140, 144, 150, 150, 150, 150, 152, 154, 156, 158, 164, 170, 170, 170, 170, 170, 170, 176, 180, 180, 180, 184, 190, 190, 190, 194, 196, 200, 204, 210, 216, 220, 224, 228, 232, 234, 238, 240, 240, 240, 240, 240, 240, 240 };
            double[] mag_i_ranges = new double[90];
            double data_for_load = 23;
            int i_for_load = 0;


            // gathering Mag I values to one array "mag_i_ranges"
            for (int i = 0; i < 45; i++)
            {
                mag_i_ranges[i_for_load] = mag_i_ranges_a[i];
                mag_i_ranges[i_for_load + 1] = mag_i_ranges_b[i];
                i_for_load += 2;
            }

            // filling up M. Mag I array with the values from 23 to 34 M. Mag
            i_for_load = 0;
            do
            {
                m_mag_ranges[i_for_load] = data_for_load;
                data_for_load += 0.25;
                i_for_load++;
            } while (data_for_load <= 34);

            //checking whether the Mag I is not out of range for specific M. Mag values
            i_for_load = 0;
            if (mmag < 23 || mmag > 34)
            {
                messageBoxFrame.Fill = new SolidColorBrush(Color.FromRgb(255, 92, 0));
                messageBox.Text = "M. Mag out of range";
            }
            else
            {
                if (mmag < 34)
                {
                    for (int i = 0; i < 45; i++)
                    {
                        if (mmag >= m_mag_ranges[i] && mmag < m_mag_ranges[i + 1])
                        {
                            if (magi < mag_i_ranges[i_for_load])
                            {                                
                                messageBoxFrame.Fill = new SolidColorBrush(Color.FromRgb(255, 92, 0));
                                messageBox.Text = "The lowest Mag I value for this M. Mag is " + System.Convert.ToString(mag_i_ranges[i_for_load]);
                            }
                            if (magi > mag_i_ranges[i_for_load + 1])
                            {
                                messageBoxFrame.Fill = new SolidColorBrush(Color.FromRgb(255, 92, 0));
                                messageBox.Text = "The highest Mag I value for this M. Mag is " + System.Convert.ToString(mag_i_ranges[i_for_load + 1]);
                            }
                        }
                        i_for_load += 2;
                    }
                }
                //this is the case when mmag is highest than 34 inclusive
                else
                {
                    if (magi <= mag_i_ranges[88])
                    {
                        messageBoxFrame.Fill = new SolidColorBrush(Color.FromRgb(255, 92, 0));
                        messageBox.Text = "The lowest Mag I value for this M. Mag is " + System.Convert.ToString(mag_i_ranges[88]);
                    }
                    if (magi >= mag_i_ranges[89])
                    {
                        messageBoxFrame.Fill = new SolidColorBrush(Color.FromRgb(255, 92, 0));
                        messageBox.Text = "The highest Mag I value for this M. Mag is " + System.Convert.ToString(mag_i_ranges[89]);
                    }
                }
            }
        }

        private void PowerLimitsCheck()
        {
            if (System.Convert.ToDouble(Power.Text) > 7 && System.Convert.ToDouble(PowerPRF.Text) > 7)
            {
                Power.Background = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                messageBoxFrame.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                messageBox.Text = "You reached maximum Magnetron power!";
            }
            else
            {
                Power.Background = new SolidColorBrush(Color.FromRgb(255,255,255)); 
            }

            if (System.Convert.ToDouble(Power.Text) > 7)
            {
                Power.Background = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                if (messageBox.Text != "You reached maximum Magnetron power!")
                {
                    messageBox.Text += " - Your nominal power is exceeded!";
                }
            }
            else
            {
                Power.Background = new SolidColorBrush(Color.FromRgb(255, 255, 255));
            }

            if (System.Convert.ToDouble(PowerPRF.Text) > 7)
            {
                PowerPRF.Background = new SolidColorBrush(Color.FromRgb(255, 0, 0));
            }
            else
            {
                PowerPRF.Background = new SolidColorBrush(Color.FromRgb(255, 255, 255));
            }
        }

        private void Calculate()
        {
            // checking which power mode is selected
            int pps = 0;
            for (int i = 0; i < 2; i++)
            {
                if (System.Convert.ToBoolean(select200.IsChecked))
                {
                    pps = 200;
                }

                if (System.Convert.ToBoolean(select400.IsChecked))
                {
                    pps = 400;
                }
            }

            //reading Mag I, M. Mag and PRF values from input fields fields
            double.TryParse(MagI.Text, out double magi);
            double.TryParse(MMag.Text, out double mmag);
            double.TryParse(PRF.Text, out double prf);

            //checking whether input doesent contains other letters than numbers
            int char_test = 0;
            for (int i = 0; i < PRF.Text.Length; i++)
            {

                char_test += System.Convert.ToInt16(!char.IsDigit(PRF.Text[i]));

            }

            for (int i = 0; i < MMag.Text.Length; i++)
            {
                if (MMag.Text[i] != System.Convert.ToChar(Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator) )
                {
                    char_test += System.Convert.ToInt16(!char.IsDigit(MMag.Text[i]));
                }
            }

            for (int i = 0; i < MagI.Text.Length; i++)
            {
                if (MagI.Text[i] != System.Convert.ToChar(Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator) )
                {
                    char_test += System.Convert.ToInt16(!char.IsDigit(MagI.Text[i]));
                }
            }


            if (pps != 0)
            {
                if (char_test != 0 )
                {
                    messageBoxFrame.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                    messageBox.Text = "Please correct provided input...";
                }
                else
                {
                    messageBox.Text = "Results are within the magnetron power limits";
                    messageBoxFrame.Fill = new SolidColorBrush(Color.FromRgb(162, 162, 162));

                    //checking input in the case of exceeding values ranges
                    CheckInput(magi, mmag);

                    // calculating power
                    Power.Text = CalculatePower(magi, mmag, pps);
                    double.TryParse(Power.Text, out double power);
                    PowerPRF.Text = CalculatePowerPRF(power, prf);

                    PowerLimitsCheck();
                }

            }
            else
            {
                messageBoxFrame.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                messageBox.Text = "Select appropriate Magnetron power mode!";
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            MessageBox.Show("Make sure, while setting up the energies you are matching them as close as possible to the factory settings.", "Important NOTICE", MessageBoxButton.OK, MessageBoxImage.Information);
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Calculate();
        }

        private void MagI_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (System.Convert.ToString(e.Key) == "Return")
            {
                Calculate();
            }
        }

        private void MMag_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (System.Convert.ToString(e.Key) == "Return")
            {
                Calculate();
            }
        }

        private void PRF_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (System.Convert.ToString(e.Key) == "Return")
            {
                Calculate();
            }
        }

        private void Window_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (System.Convert.ToString(e.Key) == "Return")
            {
                Calculate();
            }
        }
    }
}
