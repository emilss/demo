/*
 * Miscellaneous.h
 *
 *  Created on: 26 kwi 2017
 *      Author: emils
 */


#ifndef MISCELLANEOUS_H_
#define MISCELLANEOUS_H_
#include "stdlib.h"

/*
 * MISC
 */
#define ZERO 0

/**
 * UART variables and structures
 */
#define ANGLE_DIVIDER 2
// Send and receive structure
typedef struct {
	int8_t sent_buffer_count_in;
	int8_t sent_buffer_count_out;
	int8_t sent_buffer_count_difference;
	int8_t received_buffer_count_in;
	int8_t received_buffer_count_out;
	int8_t received_buffer_count_difference;
	char received_chars[32];
	char sent_chars[32];
}Uart_Transmitted_Data_Structure;

Uart_Transmitted_Data_Structure Uart_Transmitted_Data;

void Init_Uart_Transmitted_Data_Structure (Uart_Transmitted_Data_Structure * pointer);
void Put_Char_To_Send (char * chars_table, uint8_t size);
void Send_Chars ();

typedef struct {
	int angle;
	uint8_t direction;
	int l_wheel;
	int r_wheel;
	char auto_switch;
	uint8_t on_off;
} Remote_Control_Valuse_Structure;

Remote_Control_Valuse_Structure Remote_Control_Valuse;

void Set_Remote_Values ();

/**
 * This function can be used to set up the I2C1 peripheral
 * Function gives access to register specified by user
 */
#define I2C_SLAVE_ADDRESS 0xD0
#define PWR_MGMT_1_REG 0x6B
#define ACCEL_CONFIG_REG 0x1C
#define GYRO_CONFIG_REG 0x1B
#define I2C_SLAVE_TURN_OFF_TEMPERATURE_SENSOR 0x08
#define I2C_SLAVE_ACCEL_RANGE 0x08
#define I2C_SLAVE_GYRO_RANGE 0x10

void Setup_I2C1 (
							 	uint8_t slave_address,		/* your slave address */
								uint8_t register_address, 	/* register address which you want to set */
								uint8_t value_to_set );		/* value which you want to set in selected register */


/**
 * This function read all six data registers where X,Y,Z values are presents.
 * As parameters user must to pass, slave address, the first address of register which will be used for star reading data and
 * pointer to tab where the received data will be stored.
 */
// table with received data, divided by low and high bytes...1 is a high byte 2 is a low byte
uint8_t received_data[14];

#define I2C_SLAVE_DATA_REGISTER_ADDRESS 0x3B
#define I2C_HOW_MANY_REG_TO_READ 14

void Receive_Data_From_Accelerometer_At_I2C1 (uint8_t slave_address, uint8_t data_register_address,  uint8_t * tab, int how_many_registers);

/**
 * This function converts the data received from accelerometer in to values of acceleration with sign.
 * user must to pass as parameters pointer to the tab with received data and pointer to the struct
 * where converted data and sign will be stored. The default structure is declared in *.h file. The name of struct is 'My_accel_data'.
 */
#define SIZE_OF_TABE_CONSIST_ACCEL_DATA 7
#define TRANSLATION_ACCEL_DATA_GYRO_DIVIDER 32.8
#define TRANSLATION_ACCEL_DATA_ACCEL_DIVIDER 8192
#define TRANSLATION_ACCEL_DATA_SEPARATOR 4

typedef struct {
	float signed_result[SIZE_OF_TABE_CONSIST_ACCEL_DATA];
	float result[SIZE_OF_TABE_CONSIST_ACCEL_DATA];
}
My_accel_data;
// structure where are stored values after 2's compliment conversion
My_accel_data my_accel_data;
void Translation_Accel_Data_To_Digits_With_Sign (uint8_t * data_tab, int connected_received_data_tab_size, float accel_divider, float gyro_divider, int accel_gyro_separator, My_accel_data * accel_data);

/*
 * PID Function
 */
float PID (float given_value, float current_value, float time_between_measurement_of_current_value,  float proportional_amplification, float integral_amplification, float derivative_amplification, float error_limit );


/**
 * Diode control
 */
void Diode_Blinking (uint8_t direction, int time, int a, int b, int c, int d, int e, int f);
void Diode_On_Off (int on_off);

/**
 * Uart communication variables
 */

/**
 * ADC variables
 */
float Battery_ADC;

/*
 * Floating Average
 */
float FloatngAverage(float x);



#endif /* MISCELLANEOUS_H_ */
