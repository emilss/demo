/*
 * Initialization.h
 *
 *  Created on: 26 kwi 2017
 *      Author: emils
 */

#ifndef INITIALIZATION_H_
#define INITIALIZATION_H_


/**
 * Initialization of used USART port which is used for communication with BT HC06 device
 */
#define UART_TX_PIN GPIO_Pin_10
#define UART_RX_PIN GPIO_Pin_11
#define UART_BAUD_RATE 115200

void USART_Initialization();

/**
 * Initialization of used TMERS and ports which are used for controlling PWM
 */
TIM_TimeBaseInitTypeDef tim;
TIM_OCInitTypeDef  channel;

#define PWM_BRIDGE_AIN1 GPIO_Pin_0
#define PWM_BRIDGE_AIN2 GPIO_Pin_1
#define PWM_BRIDGE_BIN1 GPIO_Pin_2
#define PWM_BRIDGE_BIN2 GPIO_Pin_3
#define PWM_CONTROL_PORT GPIOA

#define PWM_OUTPUT_L GPIO_Pin_8
#define PWM_OUTPUT_R GPIO_Pin_9
#define PWM_OUTPUT_PORT GPIOB

#define PWM_Bridge_STBY_A_B GPIO_Pin_10
#define PWM_PORT_Bridge_STBY_A_B GPIOC

void TIMERS_Initialization ();
void Tim_ESetup_Period(uint8_t period_l, uint8_t period_r);

/**
 * Initialization of used output ports, generally for LEDs
 */
GPIO_InitTypeDef gpio;
void GPIO_Initialization ();

/**
 * Initialization of I2C interface for communication with accelerometer_gyroscope
 */
#define I2C_CLOCK 100000
#define I2C_SCL_PIN GPIO_Pin_6
#define I2C_SDA_PIN GPIO_Pin_7
I2C_InitTypeDef i2c;
void I2C_Initialization (int clock_speed, int SCL_pin, int SDA_pin);

/**
 * Setting up the Compliment Filter
 */
#define COMP_INIT_DELTA_T 0.005
#define COMP_INIT_TAU 0.4

/**
 * Auxiliary variables for getting angle
 */
#define VALUES_FROM_TRANSLATION signed_result
double X_angle;
double X_angle_degrees; // variable where is stored angle  x axis and ground

/**
 * Auxiliary variables for PID
 */
#define ANGEL_VALUE 0
int angel_value;
#define PWM_DUTY_VALUE 0
int16_t PWM_DUTY;

/**
 * Initialization of ADC
 */
void ADC_Initialization ();

#endif /* INITIALIZATION_H_ */
