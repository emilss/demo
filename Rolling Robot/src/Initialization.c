/*
 * Initialization.c
 *
 *  Created on: 26 kwi 2017
 *      Author: emils
 */
#include "stm32f10x.h"
#include <Initialization.h>


/**
 * Initialization of used USART port which is used for communication with BT HC06 device
 */
void USART_Initialization()
{
	// connecting proper clock signal
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB , ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);

	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = UART_TX_PIN;
	gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOB, &gpio);

	gpio.GPIO_Pin = UART_RX_PIN;
	gpio.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOB, &gpio);

	// setting up the USART interface and turning it on
	USART_InitTypeDef uart;

	USART_StructInit(&uart);
	uart.USART_BaudRate = UART_BAUD_RATE;
	USART_Init(USART3, &uart);

	// interrupt initialization
	NVIC_InitTypeDef nvic;
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	nvic.NVIC_IRQChannel = USART3_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = 0;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic);

	USART_Cmd(USART3, ENABLE);

	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
	USART_ITConfig(USART3, USART_IT_TXE, DISABLE);
}

/**
 * Initialization of used TIMERS and ports which are used for controlling PWM
 */
void TIMERS_Initialization ()
{
	// connecting proper clock signal
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC , ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);


	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = PWM_OUTPUT_L | PWM_OUTPUT_R ; // PWM outputs
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(PWM_OUTPUT_PORT, &gpio);

	gpio.GPIO_Pin = PWM_BRIDGE_AIN1 | PWM_BRIDGE_AIN2 | PWM_BRIDGE_BIN1 | PWM_BRIDGE_BIN2; // PWM outputs
	gpio.GPIO_Speed = GPIO_Speed_2MHz;
	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(PWM_CONTROL_PORT, &gpio);

	gpio.GPIO_Pin = PWM_Bridge_STBY_A_B;
	GPIO_Init(PWM_PORT_Bridge_STBY_A_B, &gpio);

	TIM_TimeBaseStructInit(&tim);
	tim.TIM_CounterMode = TIM_CounterMode_Up;
	tim.TIM_Prescaler = 16 - 1;
	tim.TIM_Period = 256 - 1;
	TIM_TimeBaseInit(TIM4, &tim);	// used Timer 4

	TIM_OCStructInit(&channel);
	channel.TIM_OCMode = TIM_OCMode_PWM1;	// Timer mode
	channel.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OC3Init(TIM4, &channel);
	TIM_OC4Init(TIM4, &channel);

	TIM_Cmd(TIM4, ENABLE);

}
void Tim_ESetup_Period(uint8_t period_l, uint8_t period_r)
{
	TIM_SetCompare3(TIM4, period_l);
	TIM_SetCompare4(TIM4, period_r);
}

/**
 * Initialization of used output ports, generally for LEDs, and PWM
 */
void GPIO_Initialization ()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB, ENABLE);
	GPIO_InitTypeDef gpio;
	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio.GPIO_Speed = GPIO_Speed_10MHz;

	gpio.GPIO_Pin = GPIO_Pin_6|GPIO_Pin_5|GPIO_Pin_4|GPIO_Pin_3|GPIO_Pin_2|GPIO_Pin_1|GPIO_Pin_0;
	GPIO_Init(GPIOC, &gpio);

	gpio.GPIO_Pin = I2C_SCL_PIN|I2C_SDA_PIN;
	GPIO_Init(GPIOB, &gpio);
	GPIO_SetBits(GPIOB, I2C_SCL_PIN|I2C_SDA_PIN);


}

/**
 * Initialization of I2C interface for communication with accelerometer_gyroscope
 */
void I2C_Initialization (int clock_speed, int SCL_pin, int SDA_pin)

{
	//turning on appropriate clock signal
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

	// setting up specified pins
	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = SCL_pin|SDA_pin;
	gpio.GPIO_Mode = GPIO_Mode_AF_OD;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &gpio);

	GPIO_SetBits(GPIOB, SCL_pin|SDA_pin); // Putting the output pins in to high state

	// setting up i2c communication
	i2c.I2C_Mode = I2C_Mode_I2C;
	i2c.I2C_DutyCycle = I2C_DutyCycle_2;
	i2c.I2C_OwnAddress1 = 0x00;
	i2c.I2C_Ack = I2C_Ack_Enable;
	i2c.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	i2c.I2C_ClockSpeed = clock_speed ;
	I2C_Init(I2C1, &i2c);
}

/**
 * Initialization of ADC
 */
void ADC_Initialization ()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2, ENABLE);
	ADC_InitTypeDef adc;

	ADC_StructInit(&adc);
	adc.ADC_ContinuousConvMode = ENABLE;
	adc.ADC_NbrOfChannel = 1;
	adc.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_Init(ADC2, &adc);

	ADC_RegularChannelConfig(ADC2, ADC_Channel_4, 1, ADC_SampleTime_28Cycles5);

	ADC_Cmd(ADC2, ENABLE);

	ADC_SoftwareStartConvCmd(ADC2, ENABLE);

//	ADC_ResetCalibration(ADC2);
//	while (ADC_GetResetCalibrationStatus(ADC2));

//	ADC_StartCalibration(ADC2);
//	while (ADC_GetCalibrationStatus(ADC2));

}



