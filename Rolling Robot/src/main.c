/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include "stm32f10x.h"
#include "Initialization.h"
#include "Miscellaneous.h"
#include "six_axis_comp_filter.h"
			

int main(void)
{
	char batt_low[9]={'l','o','w',' ','b','a','t','t','\n'};
	char led_on[6] = {'l','e','d',' ','1','\n'};
	char led_off[6] = {'l','e','d',' ','0','\n'};
	char kat[4] = {'k','a','t',':'};
	char pwm[4] = {'p','w','m',':'};
	char bat[4] = {'b','a','t',':'};
	int a=0;
	int x=1;
	int batt_check = 1;

	char array[5];



	USART_Initialization();
	Init_Uart_Transmitted_Data_Structure (&Uart_Transmitted_Data);
	TIMERS_Initialization ();
	GPIO_Initialization();
	I2C_Initialization (I2C_CLOCK, I2C_SCL_PIN, I2C_SDA_PIN);
	ADC_Initialization ();


	// wait loop after GPIO initialization, this is needed for proper working of I2C device
	for (int i = 0 ; i<50000 ; i++)
	{
		;
	}

	// Initializing structure for Compliment Filter
	SixAxis Compliment_Filter_1;
	// Setting up the Compliment Filter
	CompInit(&Compliment_Filter_1, COMP_INIT_DELTA_T, COMP_INIT_TAU);

	// Initialization of startup values for angel
	X_angle=ZERO;
	X_angle_degrees=ZERO;


	//Auxiliary variables for PID
	angel_value = ANGEL_VALUE;
	PWM_DUTY = PWM_DUTY_VALUE;
	Remote_Control_Valuse.angle = ZERO;

	// Setting up the I2C device
	Setup_I2C1(I2C_SLAVE_ADDRESS, PWR_MGMT_1_REG, I2C_SLAVE_TURN_OFF_TEMPERATURE_SENSOR);
	Setup_I2C1(I2C_SLAVE_ADDRESS, ACCEL_CONFIG_REG, I2C_SLAVE_ACCEL_RANGE);
	Setup_I2C1(I2C_SLAVE_ADDRESS, GYRO_CONFIG_REG, I2C_SLAVE_GYRO_RANGE);

	GPIO_SetBits(GPIOC, PWM_Bridge_STBY_A_B); // turning on PWM, disabling standby state

	for(;;)
	{
		Battery_ADC = FloatngAverage((ADC_GetConversionValue(ADC2)*3.3/4096)*2.625);
		Set_Remote_Values ();
		Send_Chars ();
		Receive_Data_From_Accelerometer_At_I2C1(I2C_SLAVE_ADDRESS, I2C_SLAVE_DATA_REGISTER_ADDRESS, received_data, I2C_HOW_MANY_REG_TO_READ );
		Translation_Accel_Data_To_Digits_With_Sign(received_data,I2C_HOW_MANY_REG_TO_READ/2, TRANSLATION_ACCEL_DATA_ACCEL_DIVIDER, TRANSLATION_ACCEL_DATA_GYRO_DIVIDER, TRANSLATION_ACCEL_DATA_SEPARATOR, &my_accel_data);
		CompAccelUpdate(&Compliment_Filter_1, my_accel_data.VALUES_FROM_TRANSLATION[0], my_accel_data.VALUES_FROM_TRANSLATION[1], my_accel_data.VALUES_FROM_TRANSLATION[2]);
		CompGyroUpdate(&Compliment_Filter_1, DEG_TO_RAD_RATIO*my_accel_data.VALUES_FROM_TRANSLATION[4], DEG_TO_RAD_RATIO*my_accel_data.VALUES_FROM_TRANSLATION[5], DEG_TO_RAD_RATIO*my_accel_data.VALUES_FROM_TRANSLATION[6]);
		if (Compliment_Filter_1.accelAngleX ==0)
		{
			CompStart(&Compliment_Filter_1);
		}
		CompUpdate(&Compliment_Filter_1);
		CompAnglesGet(&Compliment_Filter_1, &X_angle, 0);

		X_angle_degrees = CompRadiansToDegrees(X_angle);
		if (X_angle_degrees < 360 && X_angle_degrees > 180)
		{
			X_angle_degrees = X_angle_degrees - 360;
		}

		angel_value = Remote_Control_Valuse.angle;
		if (Remote_Control_Valuse.direction == 0)
		{
			angel_value = -1*angel_value;
		}
		if (Remote_Control_Valuse.direction == 1)
		{
			;
		}

		PWM_DUTY = PID(angel_value, X_angle_degrees, COMP_INIT_DELTA_T, 2, 15, 0.1, 255);

		if (PWM_DUTY < 0)
		{
			GPIO_ResetBits(GPIOA, PWM_BRIDGE_AIN2 | PWM_BRIDGE_BIN2);
			GPIO_SetBits(GPIOA, PWM_BRIDGE_AIN1 | PWM_BRIDGE_BIN1);
			PWM_DUTY = -1 * PWM_DUTY;
		}
		else
		{
			GPIO_ResetBits(GPIOA, PWM_BRIDGE_AIN1 | PWM_BRIDGE_BIN1);
			GPIO_SetBits(GPIOA, PWM_BRIDGE_AIN2 | PWM_BRIDGE_BIN2);
		}

		if(Remote_Control_Valuse.l_wheel > PWM_DUTY)
			Remote_Control_Valuse.l_wheel = PWM_DUTY;

		if(Remote_Control_Valuse.r_wheel > PWM_DUTY)
			Remote_Control_Valuse.r_wheel = PWM_DUTY;


		Tim_ESetup_Period(PWM_DUTY-Remote_Control_Valuse.l_wheel, PWM_DUTY-Remote_Control_Valuse.r_wheel);

		if (a == 100)
		{
			Put_Char_To_Send(&kat[0], sizeof(kat));
			sprintf(&array[0], "%d", angel_value);
			array[3]='\n';
			Put_Char_To_Send(&array[0], sizeof(array)-1);

			Put_Char_To_Send(&pwm[0], sizeof(pwm));
			sprintf(&array[0], "%d", PWM_DUTY);
			array[3]='\n';
			Put_Char_To_Send(&array[0], sizeof(array)-1);

			Put_Char_To_Send(&bat[0], sizeof(bat));
			sprintf(&array[0], "%.2f", Battery_ADC);
			array[4]='\n';
			Put_Char_To_Send(&array[0], sizeof(array));
			a=0;
		}
		a++;

		if(atoi(&Remote_Control_Valuse.auto_switch) == 1 && x==1)
		{
			Diode_On_Off(atoi(&Remote_Control_Valuse.auto_switch));
			Put_Char_To_Send(&led_on[0], sizeof(led_on));
			x=0;
		}

		if(atoi(&Remote_Control_Valuse.auto_switch) == 0 && x==0)
		{
			Diode_On_Off(atoi(&Remote_Control_Valuse.auto_switch));
			Put_Char_To_Send(&led_off[0], sizeof(led_off));
			x=1;
		}

		if(Battery_ADC < 6.8)
		{
			batt_check = 0;
		}

		if (batt_check == 0)
		{
			for(;;)
			{
				if (a == 1000000)
				{
					Diode_On_Off(0);
					Tim_ESetup_Period(0,0);
					Put_Char_To_Send(&batt_low[0], sizeof(batt_low));
					Send_Chars ();
					a=0;
				}
				a++;
			}
		}


	}
}
