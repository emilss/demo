/*
 * Miscellaneous.c
 *
 *  Created on: 26 kwi 2017
 *      Author: emils
 */
#include "stm32f10x.h"
#include <Miscellaneous.h>
typedef enum { false, true } bool;


/**
 * Functions which are for receiving and sending bytes via USART port
 */

// Initialization of Uart_Transmitted_Data_Structure
void Init_Uart_Transmitted_Data_Structure (Uart_Transmitted_Data_Structure * pointer)
{
	pointer->sent_buffer_count_in = 0 ;
	pointer->sent_buffer_count_out = 0 ;
	pointer->received_buffer_count_in = 0 ;
	pointer->received_buffer_count_out = 0 ;
	pointer->sent_buffer_count_difference = 0 ;
	pointer->received_buffer_count_difference = 0 ;
}

// Main function which realizing sending and gathering chars transmitted trough the bluetooth
void USART3_IRQHandler (void)
{
	if ((USART_GetFlagStatus(USART3, USART_FLAG_RXNE)) != RESET)
	{
		Uart_Transmitted_Data.received_chars[Uart_Transmitted_Data.received_buffer_count_in] = USART_ReceiveData(USART3);
		Uart_Transmitted_Data.received_buffer_count_in++ ;
		if (Uart_Transmitted_Data.received_buffer_count_in == 32)
		{
			Uart_Transmitted_Data.received_buffer_count_in = 0;
		}
	}
	if ((USART_GetFlagStatus(USART3, USART_FLAG_TXE)) != RESET)
	{
		Uart_Transmitted_Data.sent_buffer_count_difference = Uart_Transmitted_Data.sent_buffer_count_in - Uart_Transmitted_Data.sent_buffer_count_out;
		if (Uart_Transmitted_Data.sent_buffer_count_difference != 0)
		{
			USART_SendData(USART3, Uart_Transmitted_Data.sent_chars[Uart_Transmitted_Data.sent_buffer_count_out]);
			Uart_Transmitted_Data.sent_buffer_count_out++;
			if (Uart_Transmitted_Data.sent_buffer_count_out == 32)
			{
				Uart_Transmitted_Data.sent_buffer_count_out = 0;
			}
		}
		else
		{
			USART_ITConfig(USART3, USART_IT_TXE, DISABLE);
		}
	}
}

// Function which allows to put chars which will be send via bluetooth
void Put_Char_To_Send (char * chars_table, uint8_t size)
{
	for (int i = 0 ; i < size ; i++)
	{
		Uart_Transmitted_Data.sent_chars[Uart_Transmitted_Data.sent_buffer_count_in] = chars_table[i];
		Uart_Transmitted_Data.sent_buffer_count_in++;
		if ( Uart_Transmitted_Data.sent_buffer_count_in == 32 )
		{
			Uart_Transmitted_Data.sent_buffer_count_in = 0;
		}
	}
}

// Function which triggering transmission via bluetooth chars stored in send buffer
void Send_Chars ()
{
	USART_ITConfig(USART3, USART_IT_TXE, ENABLE);
}

// This is used for controlling looping in the received buffer
uint8_t Array_Jump (uint8_t acctual_possition, uint8_t jump_lenght)
{
	uint8_t jump;
	if (acctual_possition > (31 - jump_lenght))
	{
		jump = (jump_lenght + acctual_possition) - 32;
	}
	else
		jump = acctual_possition + jump_lenght;

	return jump;
}

//This clearing readed buffer positions
void Clear_Read_Buffer (uint8_t jump_lenght)
{
	for (int i = 0 ; i<jump_lenght ; i++)
	{
		Uart_Transmitted_Data.received_chars[Array_Jump (Uart_Transmitted_Data.received_buffer_count_out,i)] = '\0';
	}
}

// Function which checks if the control values are changed - must to be added error control !!
void Set_Remote_Values ()
{
	char x[4];

	Uart_Transmitted_Data.received_buffer_count_difference = Uart_Transmitted_Data.received_buffer_count_in - Uart_Transmitted_Data.received_buffer_count_out;
	if (Uart_Transmitted_Data.received_buffer_count_difference != 0)
	{
		if (Uart_Transmitted_Data.received_chars[Uart_Transmitted_Data.received_buffer_count_out] == 'a' && Uart_Transmitted_Data.received_chars[Array_Jump (Uart_Transmitted_Data.received_buffer_count_out, 3)] == '\n')
		{
			Remote_Control_Valuse.auto_switch = Uart_Transmitted_Data.received_chars[Array_Jump (Uart_Transmitted_Data.received_buffer_count_out, 2)];
			Clear_Read_Buffer(4);
			Uart_Transmitted_Data.received_buffer_count_out = Array_Jump (Uart_Transmitted_Data.received_buffer_count_out, 4);
		}
		if (Uart_Transmitted_Data.received_chars[Uart_Transmitted_Data.received_buffer_count_out] == 'o' && Uart_Transmitted_Data.received_chars[Array_Jump (Uart_Transmitted_Data.received_buffer_count_out, 3)] == '\n')
		{
			Remote_Control_Valuse.on_off = Uart_Transmitted_Data.received_chars[Array_Jump (Uart_Transmitted_Data.received_buffer_count_out, 2)];
			Clear_Read_Buffer(4);
			Uart_Transmitted_Data.received_buffer_count_out = Array_Jump (Uart_Transmitted_Data.received_buffer_count_out, 4);
		}
		if (Uart_Transmitted_Data.received_chars[Uart_Transmitted_Data.received_buffer_count_out] == 'd' && Uart_Transmitted_Data.received_chars[Array_Jump (Uart_Transmitted_Data.received_buffer_count_out, 9)] == '\n')
		{
			for (int i = 0 ; i<3 ; i++)
			{
				for (int j = 48 ; j<=57 ; j++)
				{
					if (Uart_Transmitted_Data.received_chars[Array_Jump (Uart_Transmitted_Data.received_buffer_count_out, 2 + i)] == j)
					{
						x[i] = j;
					}
				}
			}
			x[3] = '\0';

			if (atoi(&x[0]) < 351)
			{
				Remote_Control_Valuse.l_wheel = 255 - (atoi(&x[0]) - 100);

			}

			if (atoi(&x[0]) > 359)
			{
				Remote_Control_Valuse.r_wheel = (atoi(&x[0]) - 355);

			}
			if (atoi(&x[0]) >= 351 && atoi(&x[0]) <= 359)
			{
				Remote_Control_Valuse.r_wheel = 0;
				Remote_Control_Valuse.l_wheel = 0;
			}



			for (int i = 0 ; i<3 ; i++)
			{
				for (int j = 48 ; j<=57 ; j++)
				{
					if (Uart_Transmitted_Data.received_chars[Array_Jump (Uart_Transmitted_Data.received_buffer_count_out,  6 + i)] == j)
					{
						x[i] = j;
					}
				}
				}
			x[3] = '\0';

			if (atoi(&x[0])>265)
			{
				Remote_Control_Valuse.angle = (int)(atoi(&x[0]) - 260)/ANGLE_DIVIDER;
				Remote_Control_Valuse.direction = 0;
			}

			if (atoi(&x[0])<255)
			{
				Remote_Control_Valuse.angle = (int)(260 - atoi(&x[0]))/ANGLE_DIVIDER;
				Remote_Control_Valuse.direction = 1;
			}
			if (atoi(&x[0]) <= 265 && atoi(&x[0]) >= 255)
			{
				Remote_Control_Valuse.angle = 0;
			}

			Clear_Read_Buffer(10);
			Uart_Transmitted_Data.received_buffer_count_out = Array_Jump (Uart_Transmitted_Data.received_buffer_count_out, 10);
		}
	}
}

/**
 * This function can be used to set up the I2C1 peripheral
 * Function gives access to register specified by user
 */
void Setup_I2C1 (
							 	uint8_t slave_address,		/* your slave address */
								uint8_t register_address, 	/* register address which you want to set */
								uint8_t value_to_set )		/* value which you want to set in selected register */
	{

    I2C_GenerateSTART(I2C1, ENABLE);
    while (I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT) != SUCCESS);

    I2C_Send7bitAddress(I2C1, slave_address, I2C_Direction_Transmitter);
    while (I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) != SUCCESS);

    I2C_SendData(I2C1, register_address);
    while (I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTING) != SUCCESS);

    I2C_SendData(I2C1, value_to_set);
    while (I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTING) != SUCCESS);

    I2C_GenerateSTOP(I2C1, ENABLE);
    while(I2C_GetFlagStatus(I2C1, I2C_FLAG_STOPF));
	}

/**
 * This function read all six data registers where X,Y,Z values are presents.
 * As parameters user must to pass, slave address, the first address of register which will be used for star reading data and
 * pointer to tab where the received data will be stored.
 */
void Receive_Data_From_Accelerometer_At_I2C1 (uint8_t slave_address, uint8_t data_register_address,  uint8_t * tab, int how_many_registers)
{
	int i;
    /* Enable the I2C peripheral */
	/*======================================================*/
    I2C_GenerateSTART(I2C1, ENABLE);
    /* Test on start flag */
    while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));

    I2C_AcknowledgeConfig(I2C1, ENABLE);
   /* Test on BUSY Flag */
//    while (I2C_GetFlagStatus(I2C1,I2C_FLAG_BUSY));

    /* Send device address for write */
    I2C_Send7bitAddress(I2C1, slave_address, I2C_Direction_Transmitter);
    /* Test on master Flag */
    while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
    /* Send the device's internal address to write to */
    I2C_SendData(I2C1,data_register_address);
    /* Test on TXE FLag (data sent) */
    while (!I2C_GetFlagStatus(I2C1,I2C_FLAG_TXE));
	/*=====================================================*/
    I2C_GenerateSTART(I2C1, ENABLE);
	/* Test start flag */
    while (!I2C_GetFlagStatus(I2C1,I2C_FLAG_SB));
    /* Send address for read */
    I2C_Send7bitAddress(I2C1, slave_address, I2C_Direction_Receiver);
    /* Test Receive mode Flag */
    while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));

    for (i = 0 ; i<how_many_registers-1 ; i++)
    {
        while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_RECEIVED));
        tab[i] = I2C_ReceiveData(I2C1);
    }

    I2C_AcknowledgeConfig(I2C1, DISABLE);
    I2C_GenerateSTOP(I2C1, ENABLE);
    //while(I2C_GetFlagStatus(I2C1, I2C_FLAG_STOPF));

    while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_RECEIVED));
    tab[how_many_registers-1] = I2C_ReceiveData(I2C1);

}

/**
 * This function converts the data received from accelerometer in to values of acceleration with sign.
 * user must to pass as parameters pointer to the tab with received data and pointer to the struct
 * where converted data and sign will be stored. The default structure is declared in *.h file. The name of struct is 'My_accel_data'.
 */
void Translation_Accel_Data_To_Digits_With_Sign (uint8_t * data_tab, int connected_received_data_tab_size, float accel_divider, float gyro_divider, int accel_gyro_separator, My_accel_data * accel_data)
{
	// table where are stored connected values from 'received_data' table
	uint16_t connected_received_data[connected_received_data_tab_size];

	for (int i=0 ; i<connected_received_data_tab_size ; i++)
	{
		connected_received_data[i] = (data_tab[i*2] << 8) | data_tab[(i*2)+1];
	}

	for (int i=0 ; i<connected_received_data_tab_size ; i++)
	{
		if(i<accel_gyro_separator)
		{
			if (connected_received_data[i] & 32768)
				{
					accel_data->result[i] = connected_received_data[i] ^ 65535;
					accel_data->result[i] = accel_data->result[i]/accel_divider;
					accel_data->signed_result[i] = accel_data->result[i] * (-1);
				}
				else
				{
					accel_data->result[i] = connected_received_data[i];
					accel_data->result[i] = accel_data->result[i]/accel_divider;
					accel_data->signed_result[i] = accel_data->result[i];
				}
		}
		else
		{
			if (connected_received_data[i] & 32768)
			{
				accel_data->result[i] = connected_received_data[i] ^ 65535;
				accel_data->result[i] = accel_data->result[i]/gyro_divider;
				accel_data->signed_result[i] = accel_data->result[i] * (-1);
			}
			else
			{
				accel_data->result[i] = connected_received_data[i];
				accel_data->result[i] = accel_data->result[i]/gyro_divider;
				accel_data->signed_result[i] = accel_data->result[i];
			}
		}
	}
}

/*
 * PID Function
 */
float PID (float given_value, float current_value, float time_between_measurement_of_current_value,  float proportional_amplification, float integral_amplification, float derivative_amplification, float error_limit )
{
	// variable declaration
	float deviation;
	float proportional_result, derivative_result, error;
	static float previous_deviation, integral_result = 0 ;

	//calculation of deviation
	deviation = given_value - current_value;

	// proportional member
	proportional_result = deviation * proportional_amplification;

	// integral member
	integral_result += (((deviation + previous_deviation) / 2) * time_between_measurement_of_current_value ) * integral_amplification;
    if (integral_result > error_limit)
    {
    	integral_result = error_limit;
    }
    if (integral_result < (-1*error_limit))
    {
    	integral_result = -error_limit;
    }

	// derivative member
	derivative_result = ((deviation - previous_deviation) / time_between_measurement_of_current_value ) * derivative_amplification;

	// storing deviation which will be used as previous deviation
	previous_deviation = deviation;

	// assuming all results
	error = proportional_result + integral_result + derivative_result;
    if (error > error_limit)
    {
        error = error_limit;
    }
    if (error < (-1*error_limit))
    {
        error = -error_limit;
    }

	return error;;
}

/**
 * Diode control
 */
void Diode_Blinking (uint8_t direction, int time, int a, int b, int c, int d, int e, int f)
{
		if(direction == 1)
		{
			if (time == a)
				{
					GPIO_SetBits(GPIOC, GPIO_Pin_0);
					GPIO_ResetBits(GPIOC, GPIO_Pin_5 );
				}
				if (time == b)
				{
					GPIO_SetBits(GPIOC, GPIO_Pin_1);
					GPIO_ResetBits(GPIOC, GPIO_Pin_0 );
				}
				if (time == c)
				{
					GPIO_SetBits(GPIOC, GPIO_Pin_2);
					GPIO_ResetBits(GPIOC, GPIO_Pin_1 );
				}
				if (time == d)
				{
					GPIO_SetBits(GPIOC, GPIO_Pin_3);
					GPIO_ResetBits(GPIOC, GPIO_Pin_2 );
				}
				if (time == e)
				{
					GPIO_SetBits(GPIOC, GPIO_Pin_4);
					GPIO_ResetBits(GPIOC, GPIO_Pin_3 );
				}
				if (time == f)
				{
					GPIO_SetBits(GPIOC, GPIO_Pin_5);
					GPIO_ResetBits(GPIOC, GPIO_Pin_4 );
				}
		}
		if(direction == 0)
		{
			if (time == a)
				{
					GPIO_SetBits(GPIOC, GPIO_Pin_0);
					GPIO_ResetBits(GPIOC, GPIO_Pin_1 );
				}
				if (time == b)
				{
					GPIO_SetBits(GPIOC, GPIO_Pin_1);
					GPIO_ResetBits(GPIOC, GPIO_Pin_2 );
				}
				if (time == c)
				{
					GPIO_SetBits(GPIOC, GPIO_Pin_2);
					GPIO_ResetBits(GPIOC, GPIO_Pin_3 );
				}
				if (time == d)
				{
					GPIO_SetBits(GPIOC, GPIO_Pin_3);
					GPIO_ResetBits(GPIOC, GPIO_Pin_4 );
				}
				if (time == e)
				{
					GPIO_SetBits(GPIOC, GPIO_Pin_4);
					GPIO_ResetBits(GPIOC, GPIO_Pin_5 );
				}
				if (time == f)
				{
					GPIO_SetBits(GPIOC, GPIO_Pin_5);
					GPIO_ResetBits(GPIOC, GPIO_Pin_0 );
				}
		}


}
void Diode_On_Off (int on_off)
{
	if (on_off == 1)
	{
		GPIO_SetBits(GPIOC, GPIO_Pin_0);
		GPIO_SetBits(GPIOC, GPIO_Pin_1);
		GPIO_SetBits(GPIOC, GPIO_Pin_2);
		GPIO_SetBits(GPIOC, GPIO_Pin_3);
		GPIO_SetBits(GPIOC, GPIO_Pin_4);
		GPIO_SetBits(GPIOC, GPIO_Pin_5);
	}
	else
	{
		GPIO_ResetBits(GPIOC, GPIO_Pin_0);
		GPIO_ResetBits(GPIOC, GPIO_Pin_1);
		GPIO_ResetBits(GPIOC, GPIO_Pin_2);
		GPIO_ResetBits(GPIOC, GPIO_Pin_3);
		GPIO_ResetBits(GPIOC, GPIO_Pin_4);
		GPIO_ResetBits(GPIOC, GPIO_Pin_5);
	}

}

/*
 * Floating Average
 */

float FloatngAverage(float x)
{

	static int counter = 0;
	static int divider = 0;
	static float y[50] = {};

	if (counter == 50)
		counter = 0;

	y[counter] = x;
	float aver = 0;

	for (int i=0 ; i<50 ;i++)
	{
		aver+=y[i];
	}

	counter++;
	if( divider < 50)
	divider++;

	return aver/divider;
}
