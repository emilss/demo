# --- developer section
# ---

def WRITE_LOG(message: str):
    log = open('log.txt', 'a')
    log.write(str(datetime.datetime.now()) + ' :')
    log.write(message)
    log.write('\n')
    log.close()


def PagesToText(pdf_patch: str):
    result = []
    page_nr = 0
    PdfFile = PyPDF2.PdfFileReader(open(pdf_patch,'rb'))      # create object with pdf
    __ProgressAimUpdate__(PdfFile.getNumPages())
    for pages_amount in range(0, PdfFile.getNumPages()):            # loop in all pages in the pdf file
        page_nr = page_nr + 1
        __ProgressCountUpdate__(page_nr)
        PdfFilePage = PdfFile.getPage(pages_amount)                 # get pdf file page
        extracted_text = PdfFilePage.extractText()                  # extract all text from pdf file
        extracted_text = extracted_text.replace("\n", "")           # remove all "\n" from the text as the words might be brake by "\n"
        result.append(extracted_text)
    return result



def FindOnPage(search_for: str, page: str, space_betwene_search_and_value: int, how_many_chars: int):
    result = ""
    if page.find(search_for) != -1:                             # check whether there is a phrase in the text if no skip
        found_value = ""                                        # initialize variable to store found results
        for i in range (page.find(search_for) + len(search_for) + space_betwene_search_and_value,  page.find(search_for) + len(search_for) + space_betwene_search_and_value + how_many_chars): # loop further than found fraze to get it's value
            if i >= len(page):                                  # checking whther the "i" index is not going out of the range. It might be that the check for the value will be more that the number of characters in page
                i = len(page)-1                                 # if the range is higher than number of characters on page then the "i" index is set to the number of character on page
            found_value += page[i]                              # save text in the variale
        res = regex.findall(r"[-+]?\d*\.\d+|\d+", found_value)  # get only digits from the text
        if len(res) != 0:                                       # if no digits skip
                result = (search_for + ": " + res[0])           # save name of seek value and retrived value itself
    return result                                               # return result

def CheckFirstPageForFacilityName (seek_word: str, text: str):
    if text.find(seek_word) != -1:
        return 1
    else:
        return 0

def ExtractPdfPatchs (pdf_files_directory: str):
    pdf_file_patchs = glob.glob(pdf_files_directory + "\\*.pdf")    # retrive all files patchs from the location
    if pdf_file_patchs == []:   # check whether there are any patches retrived, if no it means there is no pdf files inside
        return 0  # return 0 if no pdf file in the patch
    return pdf_file_patchs

def ExtractNumberOfPagesFromAllFiles (pdf_file_patchs: list):
    pages_amount = 0            # initialize variable to store ammount of pages from all psf documents
    for pdf_file_patch in pdf_file_patchs:                              # loop betwene all pdf files
        PdfFile = PyPDF2.PdfFileReader(open(pdf_file_patch,'rb'))       # open to read pdf file
        pages_amount = pages_amount + PdfFile.getNumPages()             # get amount of pages of the pdf file
    return pages_amount

def __ProgressAimUpdate__(aim):
    ui.progressBar.setProperty("maximum", aim)

    
def __ProgressCountUpdate__(count):
    ui.progressBar.setProperty("value", count)

def ShowInformation (output: str):
    if len(output) != 0 :
        ui.ShowInformation.insertPlainText(output + "\n")

import PyPDF2   # importing library to handle the pdf searching
import glob     # library to handling directories and searching for all pdf files in the directory
import regex    # library used to find numbers int and folat in the string
import urllib   # to read txt files from internet

# ---
# --- developer section

# -----Window Section----------------------------------------------------------------------------------------------------------
# -----
from PyQt5 import QtCore, QtGui, QtWidgets
import datetime
from FeedbackWindow import Ui_Feedback

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setWindowModality(QtCore.Qt.WindowModal)
        MainWindow.resize(680, 600)
        MainWindow.setMinimumSize(QtCore.QSize(680, 400))
        MainWindow.setMaximumSize(QtCore.QSize(1360, 800))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("edhr_decoder.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setIconSize(QtCore.QSize(24, 24))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setMinimumSize(QtCore.QSize(0, 24))
        self.label_2.setMaximumSize(QtCore.QSize(16777215, 24))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        self.RetriveDir = QtWidgets.QPushButton(self.centralwidget)
        self.RetriveDir.setMinimumSize(QtCore.QSize(0, 24))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.RetriveDir.setFont(font)
        self.RetriveDir.setObjectName("RetriveDir")
        self.horizontalLayout.addWidget(self.RetriveDir)
        self.ShowDir = QtWidgets.QTextBrowser(self.centralwidget)
        self.ShowDir.setMinimumSize(QtCore.QSize(0, 48))
        self.ShowDir.setMaximumSize(QtCore.QSize(16777215, 48))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.ShowDir.setFont(font)
        self.ShowDir.setObjectName("ShowDir")
        self.horizontalLayout.addWidget(self.ShowDir)
        self.verticalLayout_3.addLayout(self.horizontalLayout)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.ShowInformation = QtWidgets.QPlainTextEdit(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.ShowInformation.setFont(font)
        self.ShowInformation.setObjectName("ShowInformation")
        self.verticalLayout.addWidget(self.ShowInformation)
        self.SaveInformation = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.SaveInformation.setFont(font)
        self.SaveInformation.setObjectName("SaveInformation")
        self.verticalLayout.addWidget(self.SaveInformation)
        self.verticalLayout_3.addLayout(self.verticalLayout)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.progressBar = QtWidgets.QProgressBar(self.centralwidget)
        self.progressBar.setEnabled(True)
        self.progressBar.setProperty("value", 0)
        self.progressBar.setTextVisible(False)
        self.progressBar.setFormat("%p%")
        self.progressBar.setObjectName("progressBar")
        self.verticalLayout_4.addWidget(self.progressBar)
        self.ExtractInformation = QtWidgets.QPushButton(self.centralwidget)
        self.ExtractInformation.setMinimumSize(QtCore.QSize(0, 48))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.ExtractInformation.setFont(font)
        self.ExtractInformation.setObjectName("ExtractInformation")
        self.verticalLayout_4.addWidget(self.ExtractInformation, 0, QtCore.Qt.AlignBottom)
        self.verticalLayout_3.addLayout(self.verticalLayout_4)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.FeedbackTime = QtWidgets.QLabel(self.centralwidget)
        self.FeedbackTime.setText("")
        self.FeedbackTime.setObjectName("FeedbackTime")
        self.horizontalLayout_2.addWidget(self.FeedbackTime)
        self.MainSendFeedback = QtWidgets.QPushButton(self.centralwidget)
        self.MainSendFeedback.setObjectName("MainSendFeedback")
        self.horizontalLayout_2.addWidget(self.MainSendFeedback)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 680, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
# --- developer section
# ---  
        WRITE_LOG('expirity check')
        if (expire_date >= current_date):
            WRITE_LOG('not expired')
            self.RetriveDir.setEnabled(True)
        else:
            WRITE_LOG('expired')
            self.RetriveDir.setDisabled(True)
            self.ShowInformation.insertPlainText("Software is expired...\nContact Hardware Excellence Team")
# ---
# --- developer section
    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "eDHR Decoder"  + version ))
        self.label_2.setText(_translate("MainWindow", "eDHR\'s directory:"))
        self.RetriveDir.setText(_translate("MainWindow", "Locate eDHR files"))
        self.label.setText(_translate("MainWindow", "Extracted Information"))
        self.SaveInformation.setText(_translate("MainWindow", "Save to file"))
        self.ExtractInformation.setText(_translate("MainWindow", "Extract Information"))
        self.MainSendFeedback.setText(_translate("MainWindow", "Send Feedback"))

# --- developer section
# ---
        WRITE_LOG('setting hanlders functions to click events')
        self.RetriveDir.clicked.connect(self.RetriveDir_handler)
        self.ExtractInformation.clicked.connect(self.ExtractInformation_handler)
        self.SaveInformation.clicked.connect(self.SaveInformation_handler)
        self.MainSendFeedback.clicked.connect(self.OpenFeedback_handler)

    def RetriveDir_handler(self):
        WRITE_LOG('RetriveDir_handler')
        self.SaveInformation.setDisabled(True)
        dirURL = QtWidgets.QFileDialog.getExistingDirectory()
        self.ShowDir.setPlainText(dirURL)
        if dirURL != "":
            self.ExtractInformation.setEnabled(True)
        else:
            self.ExtractInformation.setDisabled(True)
    def ExtractInformation_handler(self):
        WRITE_LOG('ExtractInformation_handler')
        self.SaveInformation.setDisabled(True)
        self.progressBar.setEnabled(True)
        self.ShowInformation.clear()

        pdf_files_patch = ExtractPdfPatchs(self.ShowDir.toPlainText())                      #get url of selected folder
        WRITE_LOG('ExtractPdfPatchs')
        if pdf_files_patch != 0:                                                            #check whether URL is not empty
            WRITE_LOG('patch not empty')
            self.ShowInformation.insertPlainText(str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")) + "\n") #print current date and time
            files_content = []                                                              #variable to store all content from files [[page,page...][page, page...][...]]
            for pdf_patch in pdf_files_patch:                                               #get all content in all pdf files from folder URL
                files_content.append(PagesToText(pdf_patch))                                #append results from files

            patch_index = 0
            page_number = 0
            WRITE_LOG('extraction start')
            for file_content in files_content:                                              # searching in all files
                
                ShowInformation(pdf_files_patch[patch_index])                               # showing the file patch before results
                WRITE_LOG(pdf_files_patch[patch_index])
                if CheckFirstPageForFacilityName("Crawley", file_content[0]) != 0:          # checking whether its Crawley eDHR
                    
                    for seek_text in core_searched_items_XRay_energy:                       # if yes searching for text in all pages
                        for page in file_content:   

                            page_number += 1                                                # every page increase the counter by one
                            if FindOnPage(seek_text, page,1,3) != "":                       # if the text is on the page print page number
                                ShowInformation("Page: "+ str(page_number))                 # print page number
                                ShowInformation(FindOnPage(seek_text, page,1,3) + "MV")                # searchin for one text in one page
                        
                        page_number = 0                                                     # reset page counter every new text

                    for seek_text in core_searched_items_XRay:
                        for page in file_content:

                            page_number += 1                                                # every page increase the counter by one
                            if FindOnPage(seek_text, page,1,4) != "":                       # if the text is on the page print page number
                                ShowInformation("Page: "+ str(page_number))                 # print page number
                                                            
                            ShowInformation(FindOnPage(seek_text, page,1,4))

                        page_number = 0                                                     # reset page counter every new text

                    for seek_text in core_searched_items_MV:
                        for page in file_content:

                            page_number += 1                                                # every page increase the counter by one
                            if FindOnPage(seek_text, page,1,4) != "":                       # if the text is on the page print page number
                                ShowInformation("Page: "+ str(page_number))                 # print page number

                            ShowInformation(FindOnPage(seek_text, page,1,4))

                        page_number = 0                                                     # reset page counter every new text

                if CheckFirstPageForFacilityName("Beijing", file_content[0]) != 0: 
                    
                    for seek_text in core_searched_items_XRay_energy:
                        for page in file_content:

                            page_number += 1                                                # every page increase the counter by one
                            if FindOnPage(seek_text, page,16,3) != "":                       # if the text is on the page print page number
                                ShowInformation("Page: "+ str(page_number))                 # print page number
                                ShowInformation(FindOnPage(seek_text, page,16,3) + "MV")

                        page_number = 0                                                     # reset page counter every new text

                    for seek_text in core_searched_items_XRay:
                        for page in file_content:

                            page_number += 1                                                # every page increase the counter by one
                            if FindOnPage(seek_text, page,31,3) != "":                       # if the text is on the page print page number
                                ShowInformation("Page: "+ str(page_number))                 # print page number

                            ShowInformation(FindOnPage(seek_text, page,31,3))

                        page_number = 0                                                     # reset page counter every new text

                    for seek_text in core_searched_items_MV:
                        for page in file_content:
                            page_number += 1                                                # every page increase the counter by one
                            if FindOnPage(seek_text, page,21,4) != "":                       # if the text is on the page print page number
                                ShowInformation("Page: "+ str(page_number))                 # print page number

                            ShowInformation(FindOnPage(seek_text, page,21,4))

                        page_number = 0                                                     # reset page counter every new text

                patch_index += 1


            self.progressBar.setDisabled(True)
            self.progressBar.setProperty("value", 0)
            self.SaveInformation.setEnabled(True)
            WRITE_LOG('extraction finished')
        else:
            WRITE_LOG('patch error')
            self.ShowInformation.insertPlainText("No pdf file in selected folder...")

    def SaveInformation_handler(self):
        WRITE_LOG('SaveInformation_handler')
        from reportlab.pdfgen import canvas
        save_dir = QtWidgets.QFileDialog.getSaveFileName(filter="*.pdf")
        if save_dir[0] != "":
            pdf = canvas.Canvas(save_dir[0])
            pdf.setTitle("Decoded Informations from eDHR")
            pdf.setFont("Helvetica", 10)
            text_list = self.ShowInformation.toPlainText().split("\n")
            row = 0
            for line in text_list:
                pdf.drawString(50, 785 - row, line)
                row += 12
            pdf.save()

    def OpenFeedback_handler(self):
        WRITE_LOG('OpenFeedback_handler')
        self.window = QtWidgets.QWidget()
        self.ui = Ui_Feedback()
        self.ui.setupUi(self.window)
        self.window.show()
    
# ---
# --- developer section
if __name__ == "__main__":
    
# --- developer section
# ---
    WRITE_LOG('\n')
    WRITE_LOG('app executed')
    version = " v1"
    WRITE_LOG(version)
    directory = ""
    core_searched_items_XRay = ["Record XLow Magnetron Pulse Current (A)", "Record XMid Magnetron Pulse Current (A)", "Record XHigh Magnetron Pulse Current (A)"]
    core_searched_items_MV = ["Record Mag I Current (A) for 4MeV", "Record Mag I Current (A) for 6MeV", "Record Mag I Current (A) for 8MeV", "Record Mag I Current (A) for 9MeV", "Record Mag I Current (A) for 10MeV", "Record Mag I Current (A) for 12MeV", "Record Mag I Current (A) for 15MeV", "Record Mag I Current (A) for 18MeV", "Record Mag I Current (A) for 20MeV", "Record Mag I Current (A) for 22MeV"]
    core_searched_items_XRay_energy = ["Record XLow Energy", "Record XMid Energy", "Record XHigh Energy"]

    additional_searched_items = []
    all_items_to_search = []
    output = []
    no_info = []
    expire_date = datetime.datetime(2020, 7, 22)
    current_date = datetime.datetime(int(datetime.datetime.now().strftime("%Y")), int(datetime.datetime.now().strftime("%m")), int(datetime.datetime.now().strftime("%d")))

# ---
# --- developer section
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
# --- developer section
# ---
    
    WRITE_LOG('fetching txt file from server')
    ext_inf = urllib.request.Request("http://company.com/app_inf/eDHR.txt") # to fetch information txt file from server
    try: urllib.request.urlopen(ext_inf)
    except urllib.error.URLError as e:
        WRITE_LOG('fetch error')
        ui.ShowInformation.appendPlainText("")
    else:
        WRITE_LOG('fetch OK')
        for line in urllib.request.urlopen(ext_inf):
            ui.ShowInformation.insertPlainText(line.decode("utf-8"))
    ui.ShowInformation.moveCursor(QtGui.QTextCursor.Start)                  # its to move vertical scrollbar up
    
    ui.ExtractInformation.setDisabled(True)
    ui.SaveInformation.setDisabled(True)
    ui.FeedbackTime.setText("Do you need more items to pull from the eDHR files? Send your feedback.\nThis software is going to expier: 22 July, 2020")
# ---
# --- developer section
    MainWindow.show()
    sys.exit(app.exec_())
# -----
# -----Window Section---------------------------------------------------------------------------------------------------------