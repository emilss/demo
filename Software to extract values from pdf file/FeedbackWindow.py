# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'FeedbackWindow.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets

# ----- developer section
# ---
import ctypes
import os
import smtplib
from email.message import EmailMessage
from urllib.request import urlopen

def Is_Internet():

    try:
        urlopen('https://www.google.com/', timeout=10)
        return True
    except: 
        return False

def SendMessage(self):
    email_address = "admin@company.com"
    email_pass = "iGRZV_wCK2"

    msg = EmailMessage()
    msg["Subject"] = "eDHR Decoder Feedback - " + os.getlogin()
    msg["From"] = email_address
    msg["To"] = "emil.kobylinski@company.com"
    msg.add_alternative(self.Message.toHtml(),subtype = "html")

    with smtplib.SMTP_SSL("serwer2006822.home.pl", 465) as smtp:
        smtp.login(email_address, email_pass)
        smtp.send_message(msg)

# ---
# ----- developer section

class Ui_Feedback(object):
    def setupUi(self, Feedback):
        Feedback.setObjectName("Feedback")
        Feedback.resize(500, 360)
        self.verticalLayout = QtWidgets.QVBoxLayout(Feedback)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(Feedback)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.Message = QtWidgets.QTextEdit(Feedback)
        self.Message.setObjectName("Message")
        self.verticalLayout.addWidget(self.Message)
        self.SendButton = QtWidgets.QPushButton(Feedback)
        self.SendButton.setObjectName("SendButton")
        self.verticalLayout.addWidget(self.SendButton)

        self.retranslateUi(Feedback)
        QtCore.QMetaObject.connectSlotsByName(Feedback)

    def retranslateUi(self, Feedback):
        _translate = QtCore.QCoreApplication.translate
        Feedback.setWindowTitle(_translate("Feedback", "Feedback"))
        self.label.setText(_translate("Feedback", "Type your message:"))
        self.Message.setText("Type your message here, you can create formatted message with pictures using Word and paste it here.\nDelete this information and write your opinion!\n\nHardware Installation Excellence")
        self.SendButton.setText(_translate("Feedback", "Send"))

# ----- developer section
# ---
        self.SendButton.clicked.connect(self.SendMessage_handler)

    def SendMessage_handler(self):
        if Is_Internet() == True:
            SendMessage(self)
            self.SendButton.setDisabled(True)
            self.Message.setText("Your message has been sent!")
        else:
            ctypes.windll.user32.MessageBoxW(0, u"No internet connection", u"Error", 0)
# ---
# ----- developer section
