# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainWindow.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setWindowModality(QtCore.Qt.WindowModal)
        MainWindow.resize(680, 400)
        MainWindow.setMinimumSize(QtCore.QSize(680, 400))
        MainWindow.setMaximumSize(QtCore.QSize(1360, 800))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("edhr_extractor.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setIconSize(QtCore.QSize(24, 24))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setMinimumSize(QtCore.QSize(0, 24))
        self.label_2.setMaximumSize(QtCore.QSize(16777215, 24))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        self.RetriveDir = QtWidgets.QPushButton(self.centralwidget)
        self.RetriveDir.setMinimumSize(QtCore.QSize(0, 24))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.RetriveDir.setFont(font)
        self.RetriveDir.setObjectName("RetriveDir")
        self.horizontalLayout.addWidget(self.RetriveDir)
        self.ShowDir = QtWidgets.QTextBrowser(self.centralwidget)
        self.ShowDir.setMinimumSize(QtCore.QSize(0, 48))
        self.ShowDir.setMaximumSize(QtCore.QSize(16777215, 48))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.ShowDir.setFont(font)
        self.ShowDir.setObjectName("ShowDir")
        self.horizontalLayout.addWidget(self.ShowDir)
        self.verticalLayout_3.addLayout(self.horizontalLayout)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.ShowInformation = QtWidgets.QPlainTextEdit(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.ShowInformation.setFont(font)
        self.ShowInformation.setObjectName("ShowInformation")
        self.verticalLayout.addWidget(self.ShowInformation)
        self.SaveInformation = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.SaveInformation.setFont(font)
        self.SaveInformation.setObjectName("SaveInformation")
        self.verticalLayout.addWidget(self.SaveInformation)
        self.verticalLayout_3.addLayout(self.verticalLayout)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.progressBar = QtWidgets.QProgressBar(self.centralwidget)
        self.progressBar.setEnabled(True)
        self.progressBar.setProperty("value", 0)
        self.progressBar.setTextVisible(False)
        self.progressBar.setFormat("%p%")
        self.progressBar.setObjectName("progressBar")
        self.verticalLayout_4.addWidget(self.progressBar)
        self.ExtractInformation = QtWidgets.QPushButton(self.centralwidget)
        self.ExtractInformation.setMinimumSize(QtCore.QSize(0, 48))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.ExtractInformation.setFont(font)
        self.ExtractInformation.setObjectName("ExtractInformation")
        self.verticalLayout_4.addWidget(self.ExtractInformation, 0, QtCore.Qt.AlignBottom)
        self.verticalLayout_3.addLayout(self.verticalLayout_4)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setText("")
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_2.addWidget(self.label_3)
        self.MainSendFeedback = QtWidgets.QPushButton(self.centralwidget)
        self.MainSendFeedback.setObjectName("MainSendFeedback")
        self.horizontalLayout_2.addWidget(self.MainSendFeedback)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 680, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "eDHR Extractor v0.1"))
        self.label_2.setText(_translate("MainWindow", "eDHR\'s directory:"))
        self.RetriveDir.setText(_translate("MainWindow", "Locate eDHR files"))
        self.label.setText(_translate("MainWindow", "Extracted Information"))
        self.SaveInformation.setText(_translate("MainWindow", "Save to file"))
        self.ExtractInformation.setText(_translate("MainWindow", "Extract Information"))
        self.MainSendFeedback.setText(_translate("MainWindow", "Send Feedback"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
