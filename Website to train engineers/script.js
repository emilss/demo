var selected_progress = 0;
var config_file = './res/config.txt';
var tasks_numbers = [];
var tasks_order = [];
var tasks_url = [];
var tasks_name = [];
var tasks_jpg = [];
var tasks_amount = 0;
var tasks_amt_pa_tls = 8 ;
var tasks_cr_st = 0;
var tasks_cr_sp = 0;
var tasks_how_many_inst_counter = 0;
var tasks_how_many_inst = 3;
var installed_counter = 0;
var task_height = 240;
// stopwatch  variable
var SW_seconds =0;
var SW_minutes = 0;
var SW_hours = 0;
var SW_time = "0h 0m 0s";
var Stop_Watch;

/*calculating construction time*/
function StopWatch()
{
	SW_seconds++;

	if(SW_seconds == 60)
		{
			SW_minutes++;
			SW_seconds = 0;
		}
	if(SW_minutes == 60)
		{
			SW_hours++;
			SW_minutes = 0;
		}

	SW_time = SW_hours + "h " + SW_minutes + "m " + SW_seconds + "s"
	$("#time").html(SW_time);
}

/*adding e-mail frame*/
function addMailIframe() {
	var instname = $("#name").val()
	var instmessage = $("#message").val()
	$('<iframe src="mailto:installationexcellencehardware@company.com?subject=BrickNRadiate Completed - ' + instname +'&body=User name: ' + instname +' | Started from completenes of: ' + selected_progress + '%' + ' | Construction time: ' + SW_time + ' | Remarks: ' + instmessage + '">')
	   .appendTo('#myIframe')
	   .css("display", "none");
	location.reload(true)
  }

/*showing message when construction is complete*/
function ConstructionComplete()
{
	$('<div></div>')
	.attr({'id':'finish'})
	.appendTo('#workbench')
	.html('Congratulations!</br><br>You built your own Linac</br><br> You started from ' + selected_progress + '%' +' completenes of installtion.<br></br> Construction time: ' + SW_time + ' <br></br> Your full name: <br>  <input type="text" id="name"><br>  Remarks:<br>  <textarea id="message" rows="10" cols="60"></textarea> </br> <input type="button" onclick="addMailIframe();" value="Submit" /> <div id="myIframe"> </div> <br></br> By submiting a new Outlook message will be created, send that created message. Or reload the page to start over.')
}

/* Handle task drop*/
function handleTaskDrop__ (e, ui){


		tasks_how_many_inst_counter++;
		ui.draggable.appendTo('#installed');
		ui.draggable.draggable( 'disable' );
		ui.draggable.draggable( 'option', 'revert', false );
		ui.draggable.addClass('task_installed');
		ui.draggable.removeClass('task_not_installed');
		if (tasks_how_many_inst_counter == tasks_how_many_inst) {
			tasks_how_many_inst_counter = 0;
			tasks_cr_st = tasks_cr_sp;
			tasks_cr_sp = tasks_cr_sp + tasks_how_many_inst;
			TasksCreation ();
	}
	task_height = task_height + task_height;
	$('#installed').scrollTop(task_height);
	ProgressChange(((installed_counter)/tasks_amount)*100);
	installed_counter++;
	

}


/* Handle task drop checking conditions*/
function handleTaskDrop(e, ui){

	if (installed_counter == ui.draggable.data('task_number')) {

		handleTaskDrop__ (e, ui);
		
	} else if(tasks_order[ui.draggable.data('task_number')-1] == 1 && tasks_order[installed_counter-1] != 0) {

		handleTaskDrop__ (e, ui);
	}

	if (installed_counter == tasks_amount+1){
		ConstructionComplete();
		clearInterval(Stop_Watch);
	}
}


// This function only giving random values from the given range.
function getRandomInt(min, max){
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


/*Tasks Creation*/
function TasksCreation (){

	if (tasks_cr_sp + tasks_how_many_inst > tasks_amount) {
		
		tasks_cr_sp = tasks_amount;
	}
//		console.log('start: '+tasks_cr_st+'\n'+'stop: '+tasks_cr_sp );
		for (let i = tasks_cr_st; i < tasks_cr_sp; i++) {
			
			$('<div>' + '<img src= "./res/img/'+ tasks_jpg[i] + '">' + '<a href="' + tasks_url[i] + '" target="_blank" >' + tasks_name[i] + '</a></div>')
			.data('task_number', tasks_numbers[i] )
			.attr({'class':'task task_not_installed', 'id':'task_' + tasks_numbers[i] })
			.appendTo('#parts_to_use')
			.css({'top': getRandomInt(0, 60) + '%', 'left': getRandomInt(0, 60) + '%',})
			.draggable({
				containment: '#workbench', 
				stack: '#workbench div', 
				revert: false})
		}
}


/*changing progress bar*/
function ProgressChange(__prog){
	$('#inner_bar').css({'width': __prog+'%'})
	.html(Math.round(__prog)+'%');
}

/*starting the game */
function GameStart (){

	ProgressChange(selected_progress);
	$('#select_progress').remove();

	$.when($.get(config_file, function (file_content){
		file_content = file_content.split('\n');
		tasks_amount = file_content.length;
		for (let j = 0; j < tasks_amount; j++) {
			split_line = file_content[j].split(';');
			tasks_numbers.push(split_line[0]);
			tasks_order.push(split_line[1]);
			tasks_url.push(split_line[2]);
			tasks_name.push(split_line[3]);
			tasks_jpg.push(split_line[4]);
		}

		installed_counter = tasks_cr_st = Math.round(tasks_amount*(selected_progress/100));
		installed_counter = installed_counter + 1;

		for (let i = 0; i < tasks_cr_st ; i++) {
		
			$('<div>' + '<img src= "./res/img/'+ tasks_jpg[i] + '">' + '<a href="' + tasks_url[i] + '" target="_blank" >' + tasks_name[i] + '</a></div>')
			.data('task_number', tasks_numbers[i] )
			.attr({'class':'task task_installed', 'id':'task_' + tasks_numbers[i] })
			.appendTo('#installed')
			task_height = task_height + task_height;
			$('#installed').scrollTop(task_height);
		}
		tasks_cr_sp = tasks_cr_st + tasks_amt_pa_tls;
		if (tasks_cr_sp + tasks_amt_pa_tls > tasks_amount) {
		
			tasks_cr_sp = tasks_amount;
		}
		Stop_Watch = setInterval(StopWatch, 1000);
	}))
	.then(TasksCreation)
	

}

/* Handling selection of the Installation progress */
function SelectProgression(){

	$('<div>Select the progress?</div>')
	.attr({'id':'select_progress'})
	.appendTo('#workbench');
	$('<div></div>')
	.attr({'id':'slider_cont'})
	.appendTo('#select_progress');
	$('<div></div>')
	.attr({'id':'slider'})
	.appendTo('#slider_cont');
	$('<div>0%</div>')
	.attr({'id':'progress_submit'})
	.appendTo('#select_progress')
	.click(function(e, ui) {
		if (selected_progress > 98){
			selected_progress = 98;
		}
		GameStart();
	});

	$('#slider').slider({min: 0, max: 100, value: 0, slide: function(e, ui) {
		selected_progress = $(this).slider("value");
		$('#progress_submit').html(selected_progress+'%');

	}});

}


function Init(){

	
	//alert("Hello World");
	$('#installed').droppable({accept: '.task', drop: handleTaskDrop});
	SelectProgression();
	
}


